#!/usr/bin/env python3
import csv
import datetime
import hashlib
import logging
import sys
import setproctitle
import telebot
import time
import traceback
from dateutil.relativedelta import relativedelta
from multiprocessing import Process
from pony import orm
from urllib import request


SHEDULE_LINK = "https://docs.google.com/spreadsheets/d/139zViuFU2BMZpbgGGBnGq5I_V0oz6zwA-1blxQc1qG4/export?format=csv"

HELP = "Бот тип. Присылаю расписание, вот это все. Чтоб получить расписаньку, набери /schedule [название группы латиницией].\nНапример, /schedule 11a-1.\n/schedule без указания группы пришлет расписание целиком\n\nЯ еще даже не в альфе, так что у меня серьезные проблемы с функционалом.\n\nАх да, еще я бездомный, так в любой момент могу просто взять и не ответить.\n\nА еще Лева пидор."
SETDEFAULT = "Укажите список групп, расписание для которых будет выводиться по команде /schedule."
DEFAULTSET = "Готово."
SUBSCRIBED = "Вы подписались на обновления расписания."
UNSUBSCRIBED = "Вы отписались от обновлений расписания."

months_replace = {
    'января': ' 1 ',
    'февраля': ' 2 ',
    'марта': ' 3 ',
    'апреля': ' 4 ',
    'мая': ' 5 ',
    'июня': ' 6 ',
    'июля': ' 7 ',
    'августа': ' 8 ',
    'сентября': ' 9 ',
    'октября': ' 10 ',
    'ноября': ' 11 ',
    'декабря': ' 12 '
}

db = orm.Database()
bot = telebot.TeleBot('190407050:AAHxCDwLFVaynpuouOKtNqDlk9HMaTZf3H4')


class Lesson(db.Entity):
    id = orm.PrimaryKey(int, auto=True)
    name = orm.Required(str)
    index = orm.Required(int)
    room = orm.Optional(str)
    day = orm.Required('Day')


class Day(db.Entity):
    id = orm.PrimaryKey(int, auto=True)
    date = orm.Required(datetime.date)
    lessons = orm.Set('Lesson')
    group = orm.Required('Group')

    @orm.db_session
    def __str__(self):
        lessons = []
        for lesson in self.lessons:
            lessons.append("{0.index}. {0.name} ({0.room})".format(lesson))
        lessons.sort()
        return "{}:\n{}".format(self.group.name, '\n'.join(lessons))


class Group(db.Entity):
    id = orm.PrimaryKey(int, auto=True)
    name = orm.Required(str)
    days = orm.Set('Day')
    users = orm.Set('User')


class Schedule(db.Entity):
    md5 = orm.PrimaryKey(str)


class User(db.Entity):
    id = orm.PrimaryKey(str)
    groups = orm.Set('Group')
    subscribed = orm.Required(bool, default=False)

db.bind('sqlite', 'test_db.sqlite', create_db=True)
db.generate_mapping(create_tables=True)


@orm.db_session
def send_updates(date):
    users = orm.select(u for u in User if u.subscribed is True)
    days = orm.select(d for d in Day if d.date == date).prefetch(Group)
    for user in users:
        grs = user.groups or orm.select(g for g in Group)[:]
        days_u = days.filter(lambda day: day.group in grs)
        update = "Обновляшки!\nРасписание на {}:".format(date)
        for day in days_u:
            update = "{}\n{}".format(update, day)
        update = "```\n{}\n```".format(update)
        logging.error(int(user.id))
        try:
            bot.send_message(int(user.id), update, parse_mode="Markdown")
        except:
            logging.error("Couldn't send update to user {}. \n{}".format(user.id, traceback.format_exc()))


@orm.db_session
def update_schedule():
    logging.info('Updating schedule...')
    schedule_raw = request.urlopen(SHEDULE_LINK).read()
    md5 = hashlib.md5(schedule_raw).hexdigest()
    if orm.get(s for s in Schedule if s.md5 == md5):
        return
    Schedule(md5=md5)

    schedule_raw = schedule_raw.decode()

    schedule = []

    for row in csv.reader(schedule_raw.split('\n'), dialect='excel'):
        schedule.append(row)

    groups = []
    a = 0
    for i in range(len(schedule)):
        if len(list(filter(lambda x: x != '', schedule[i]))) > 0:
            a = i
            break
    for i in range(a):
        schedule.pop(0)
    date = list(filter(lambda x: x != '', schedule[0]))[0]
    for month in months_replace:
        date = date.replace(month, months_replace[month])
    date = date.replace(',', ' ')
    date = date.split()
    logging.error(date)
    date = datetime.date(int(date[3].rstrip('г.')), int(date[2]), int(date[1]))
    orm.delete(l for l in Lesson if l.day.date == date)
    orm.delete(d for d in Day if d.date == date)

    for row in schedule[2:]:
        if not row[0].isnumeric():
            groups.clear()
            for a in row:
                if a:
                    groupname = a.replace("γ", "g")
                    group = orm.get(g for g in Group if g.name == groupname)
                    if not group:
                        group = Group(name=groupname)
                    day = Day(date=date, group=group)
                    groups.append(day)
        else:
            index = int(row[0])
            lessons = list(zip(*[iter(row[1:])] * 2))
            for i in range(len(lessons)):
                if lessons[i][0]:
                    Lesson(name=lessons[i][0], room=lessons[i][1], index=index, day=groups[i])
    send_updates(date)


@bot.message_handler(commands=['schedule'])
@orm.db_session
def schedule(message):
    tomorrow_days = orm.select(d for d in Day if d.date == datetime.date.today() + relativedelta(days=1))
    if tomorrow_days:
        grs = message.text.split()[1:]
        if not grs:
            try:
                grs = orm.get(u for u in User if u.id == message.chat.id).groups
            except AttributeError:
                grs = orm.select(g for g in Group)
        else:
            grs = orm.select(g for g in Group if g.name in grs)
        tomorrow_days = tomorrow_days.filter(lambda day: day.group in grs)
        reply = "Расписание на завтра ({})".format(datetime.date.today() + relativedelta(days=1))
        for day in tomorrow_days:
            reply = "{}\n{}".format(reply, day)
        reply = "```\n{}\n```".format(reply)
        bot.send_message(message.chat.id, reply, parse_mode="Markdown")

    else:
        bot.send_message(message.chat.id, "Расписания на завтра еще нет, вот расписание на сегодня:")
        schedule_today(message)


@bot.message_handler(commands=['schedule_today'])
@orm.db_session
def schedule_today(message):
    today_days = orm.select(d for d in Day if d.date == datetime.date.today())
    if today_days:
        grs = message.text.split()[1:]
        if not grs:
            try:
                grs = orm.get(u for u in User if u.id == message.chat.id).groups
            except AttributeError:
                grs = orm.select(g for g in Group)
        else:
            grs = orm.select(g for g in Group if g.name in grs)
        today_days = today_days.filter(lambda day: day.group in grs)
        reply = "Расписание на сегодня ({})".format(datetime.date.today())
        for day in today_days:
            reply = "{}\n{}".format(reply, day)
        reply = "```\n{}\n```".format(reply)
        bot.send_message(message.chat.id, reply, parse_mode="Markdown")


@bot.message_handler(commands=['setdefault'])
def setdefault_handler(message):
    bot.send_message(message.chat.id, SETDEFAULT, reply_markup=telebot.types.ForceReply())


@bot.message_handler(func=lambda m: m.reply_to_message and m.reply_to_message.text == SETDEFAULT and m.text)
@orm.db_session
def setdefault(message):
    grs = message.text.split()
    grs = orm.select(g for g in Group if g.name in grs)
    user = orm.get(u for u in User if u.id == message.chat.id)
    if not user:
        user = User(id=str(message.chat.id), groups=grs)
    else:
        user.set(groups=grs)
    bot.send_message(message.chat.id, DEFAULTSET)


@bot.inline_handler(lambda m: True)
@orm.db_session
def inline(query):
    days = orm.select(d for d in Day if d.date == datetime.date.today() or d.date == datetime.date.today() + relativedelta(days=1))
    grs = query.query.split()
    if not grs:
        try:
            grs = orm.get(u for u in User if u.id == query.from_user.id).groups
        except AttributeError:
            grs = orm.select(g for g in Group)
    else:
        grs = orm.select(g for g in Group if g.name in grs)
    days = days.filter(lambda day: day.group in grs)
    today_days = days.filter(lambda day: day.date == datetime.date.today())
    tomorrow_days = days.filter(lambda day: day.date != datetime.date.today())
    r_today = ""
    r_tomorrow = ""
    if today_days:
        for day in today_days:
            r_today = "{}\n{}".format(r_today, day)
    if tomorrow_days:
        for day in tomorrow_days:
            r_tomorrow = "{}\n{}".format(r_tomorrow, day)
    if r_today:
        r_today = "Расписание на сегодня ({}){}".format(datetime.date.today(), r_today)
    else:
        r_today = "Ничего не найдено"
    if r_tomorrow:
        r_tomorrow = "Расписание на завтра ({}){}".format(datetime.date.today() + relativedelta(days=1), r_tomorrow)
    else:
        r_tomorrow = "Ничего не найдено."

    r_today = telebot.types.InlineQueryResultArticle(str(hash(query.query + 'today'))[:16], "Расписание на сегодня", telebot.types.InputTextMessageContent(r_today), description=r_today)
    r_tomorrow = telebot.types.InlineQueryResultArticle(str(hash(query.query + 'tomorrow'))[:16], "Расписание на завтра", telebot.types.InputTextMessageContent(r_tomorrow), description=r_tomorrow)
    bot.answer_inline_query(query.id, [r_today, r_tomorrow])
    pass


@bot.message_handler(commands=['start', 'help'])
def help(message):
    bot.send_message(message.chat.id, HELP)


@bot.message_handler(commands=['subscribe'])
@orm.db_session
def subscribe(message):
    user = orm.get(u for u in User if u.id == message.chat.id)
    if not user:
        user = User(id=str(message.chat.id), subscribed=True)
    else:
        user.set(subscribed=True)
    bot.send_message(message.chat.id, SUBSCRIBED)


@bot.message_handler(commands=['unsubscribe'])
@orm.db_session
def unsubscribe(message):
    user = orm.get(u for u in User if u.id == message.chat.id)
    if not user:
        user = User(id=str(message.chat.id), groups=grs, subscribed=False)
    else:
        user.set(subscribed=False)
    bot.send_message(message.chat.id, UNSUBSCRIBED)


def updater():
    setproctitle.setproctitle('schedulebot-updater')
    while True:
        time.sleep(60 * 20)
        update_schedule()


if __name__ == '__main__':
    setproctitle.setproctitle('schedulebot')
    while True:
        try:
            logging.basicConfig()
            logging.info('First schedule update...')
            update_schedule()
            logging.info('First schedule update run, starting bot')
            upd = Process(target=updater)
            upd.start()
            bot.polling()
        except KeyboardInterrupt:
            sys.exit(0)
        except:
            logging.error(traceback.format_exc())

